//
//  UIWindow+Utilities.m
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 14/07/2021.
//

#import "UIWindow+Utilities.h"

@implementation UIWindow (Utilities)

+ (UIInterfaceOrientation)kyc_interfaceOrientation {
    if (@available(iOS 13.0, *)) {
        UIWindow *topVisibleWindow = UIApplication.sharedApplication.windows.firstObject;
        UIInterfaceOrientation orientation = topVisibleWindow.windowScene.interfaceOrientation;
        return orientation;
    }
    return [UIApplication sharedApplication].statusBarOrientation;
}

@end
