//
//  UIImage+Utilities.h
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 14/07/2021.
//

#import <UIKit/UIKit.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Utilities)

- (CGFloat)kyc_brightnessEstimate;

- (UInt8 *)kyc_convertToBitmapRGBA8;

+ (UIImage *)kyc_cropImageFromImage:(UIImage *)image toRect:(CGRect)rect;

+ (UIImage *)kyc_imageFromSampleBuffer:(CMSampleBufferRef)sampleBuffer
                           orientation:(UIImageOrientation)orientation;

+ (UIImageOrientation)kyc_imageOrientationFromInterfaceOrientation:(UIInterfaceOrientation)orientation
                                                    cameraPosition:(AVCaptureDevicePosition)cameraPosition;

+ (UIImage *)kyc_imageScaleFromImage:(UIImage *)image toSize:(CGSize)size;

@end

NS_ASSUME_NONNULL_END
