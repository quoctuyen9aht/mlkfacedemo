//
//  KYCMacros.h
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 14/07/2021.
//

#ifndef KYCMacros_h
#define KYCMacros_h

#define WEAKSELF                __weak typeof(self) weakSelf = self;
#define STRONGSELF              __strong typeof(self) self = weakSelf; if (!self) { return; }
#define STRONGSELF_RETURN(_var) __strong typeof(self) self = weakSelf; if (!self) { return _var; }


#define safeExec(block, ...) block ? block(__VA_ARGS__) : nil

////////////////////////////////////////////////////////////////////////////////
/// Assert macros
////////////////////////////////////////////////////////////////////////////////
#if DEBUG

#define KYC_LOG(arguments, ...) NSLog(@"[KYC] " arguments, ##__VA_ARGS__)

# if TARGET_IPHONE_SIMULATOR

#define KYC_LOW_ASSERT(cond, args, ...) \
    if (!(cond)) {\
        NSLog(@"[KYC] " args, ##__VA_ARGS__);\
        NSLog(@"[KYC] METHOD: %s - LINE: %d", __FUNCTION__, __LINE__);\
        __asm__("int $3\n" : : ); \
    }
#else /* TARGET_IPHONE_SIMULATOR */
#define KYC_LOW_ASSERT(cond, args, ...) \
    if (!(cond)) {\
        NSLog(@"[KYC] " args, ##__VA_ARGS__);\
        NSLog(@"[KYC] METHOD: %s - LINE: %d", __FUNCTION__, __LINE__);\
        raise(SIGTRAP);\
    }
#endif /* TARGET_IPHONE_SIMULATOR */

#else /* DEBUG */

#define KYC_LOG(arguments, ...) {}
#define KYC_LOW_ASSERT(cond, args, ...) {}

#endif /* DEBUG */

#define KYC_MESSAGE(arguments, ...) [NSString stringWithFormat:@"[ZDS] " arguments, ##__VA_ARGS__]

#endif /* KYCMacros_h */
