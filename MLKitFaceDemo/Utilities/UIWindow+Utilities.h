//
//  UIWindow+Utilities.h
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 14/07/2021.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIWindow (Utilities)

+ (UIInterfaceOrientation)kyc_interfaceOrientation;

@end

NS_ASSUME_NONNULL_END
