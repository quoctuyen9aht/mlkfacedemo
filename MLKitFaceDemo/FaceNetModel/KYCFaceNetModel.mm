//
//  KYCFaceNetModel.m
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 15/07/2021.
//

#import "KYCFaceNetModel.h"
#import "KYCMacros.h"
#import "UIImage+Utilities.h"
#import <TFLTensorFlowLite/TFLTensorFlowLite.h>

#define modelFileName   @"mobile_face_net"
#define modelFileType   @"tflite"

#define imageWidth          112
#define imageHeight         112
#define outputLength        192
#define stdNormalizeMean    128.0f

@interface KYCFaceNetModel ()

@property(nonatomic, strong) TFLInterpreter *interpreter;

@end

@implementation KYCFaceNetModel

+ (NSString *)_filePathFromResourceName:(NSString *)name extension:(NSString *)extension {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:name ofType:extension];
    KYC_LOW_ASSERT([filePath isKindOfClass:NSString.class] && ![filePath isEqualToString:@""], @"Cannot get model path");
    return filePath;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSString *modelPath = [self.class _filePathFromResourceName:modelFileName extension:modelFileType];
        TFLInterpreterOptions *options = [[TFLInterpreterOptions alloc] init];
        options.numberOfThreads = 4;
        NSError *error;
        self.interpreter = [[TFLInterpreter alloc] initWithModelPath:modelPath
                                                             options:options
                                                               error:&error];
        KYC_LOW_ASSERT(error == nil, @"%@", error);
        [self.interpreter allocateTensorsWithError:&error];
        KYC_LOW_ASSERT(error == nil, @"%@", error);
    }
    return self;
}

- (NSArray *)extractFeaturesFromImage:(UIImage *)image {
    CGSize size = CGSizeMake(imageWidth, imageHeight);
    UIImage *imageScale = [UIImage kyc_imageScaleFromImage:image toSize:size];
    NSData *data = [self _dataWithProcessImage:imageScale];
    
    NSError *error = nil;
    TFLTensor *inputTensor = [self.interpreter inputTensorAtIndex:0 error:&error];
    KYC_LOW_ASSERT(error == nil, @"%@", error);
    [inputTensor copyData:data error:&error];
    KYC_LOW_ASSERT(error == nil, @"%@", error);
    
    [self.interpreter invokeWithError:&error];
    KYC_LOW_ASSERT(error == nil, @"%@", error);
    
    TFLTensor *outputTensor = [self.interpreter outputTensorAtIndex:0 error:&error];
    KYC_LOW_ASSERT(error == nil, @"%@", error);
    NSData *outputData = [outputTensor dataWithError:&error];
    KYC_LOW_ASSERT(error == nil, @"%@", error);
    
    float *features = new float[outputLength];
    [outputData getBytes:features length:(sizeof(float)*outputLength)];
    
    NSMutableArray *output = [NSMutableArray array];
    for (int i = 0; i < outputLength; i++) {
        NSNumber *number = [NSNumber numberWithFloat:features[i]];
        [output addObject:number];
    }
    return output;
}

- (NSData *)_dataWithProcessImage:(UIImage *)image {
    UInt8 *imageData = [image kyc_convertToBitmapRGBA8];
    float *floats = new float[imageWidth * imageHeight * 3];
    const float intputStd = stdNormalizeMean;
    int k = 0;
    int size = imageWidth * imageHeight * 4;
    for (int j = 0; j < size; j++) {
        if (j % 4 == 3) {
            continue;
        }
        floats[k] = imageData[j] / intputStd;
        k++;
    }
    free(imageData);
    
    NSData *data = [NSData dataWithBytes:floats length:sizeof(float) * imageWidth * imageHeight * 3];
    delete [] floats;
    
    return data;
}


@end
