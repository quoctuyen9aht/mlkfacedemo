//
//  KYCFaceNetModel.h
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 15/07/2021.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KYCFaceNetModel : NSObject

- (NSArray *)extractFeaturesFromImage:(UIImage *)image;

@end

NS_ASSUME_NONNULL_END
