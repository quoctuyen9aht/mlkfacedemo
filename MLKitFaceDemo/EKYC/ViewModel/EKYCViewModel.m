//
//  EKYCViewModel.m
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 14/07/2021.
//

#import "EKYCViewModel.h"
#import "UIWindow+Utilities.h"
#import "UIImage+Utilities.h"
#import "KYCMacros.h"
#import "KYCFaceNetModel.h"
@import MLKit;
@import MLKitCommon;
@import MLKitVision;
@import MLKitFaceDetection;
@import AVFoundation;

#define kEyeOpenedThreshold         0.65
#define kLowBrightnessThreshold     80
#define kHightBrightnessThreshold   220

@interface EKYCViewModel ()

@property(nonatomic, strong) MLKFaceDetector *faceDetector;
@property(nonatomic, assign) AVCaptureDevicePosition cameraPosition;
@property(nonatomic, assign) CGRect validateFrame;
@property(nonatomic, assign) CGRect cameraFrame;

@property(nonatomic, strong) KYCFaceNetModel *faceNetModel;

@end

@implementation EKYCViewModel

- (instancetype)initWithCameraPosition:(AVCaptureDevicePosition)cameraPosition
                              validateFrame:(CGRect)faceArea
                           cameraFrame:(CGRect)cameraFrame {
    self = [super init];
    if (self) {
        self.cameraPosition = cameraPosition;
        self.validateFrame = faceArea;
        self.cameraFrame = cameraFrame;
        self.faceNetModel = [[KYCFaceNetModel alloc] init];
        [self _setupMLKDetector];
    }
    return self;
}

- (void)_setupMLKDetector {
    MLKFaceDetectorOptions *options = [[MLKFaceDetectorOptions alloc] init];
    options.performanceMode = MLKFaceDetectorPerformanceModeAccurate;
    options.landmarkMode = MLKFaceDetectorLandmarkModeAll;
    options.classificationMode = MLKFaceDetectorClassificationModeAll;
    options.trackingEnabled = YES;
    self.faceDetector = [MLKFaceDetector faceDetectorWithOptions:options];
}

- (void)faceWithCMSampleBuffer:(CMSampleBufferRef)sampleBuffer
                    completion:(void (^)(KYCFace * _Nullable, KYCError * _Nullable))completion {
    UIImageOrientation imageOrientation = [UIImage kyc_imageOrientationFromInterfaceOrientation:UIInterfaceOrientationPortrait
                                                                                 cameraPosition:self.cameraPosition];
    UIImage *image = [UIImage kyc_imageFromSampleBuffer:sampleBuffer
                                            orientation:imageOrientation];
    
    KYCError *error = nil;
    error = [self _isSatisifiedBrightness:image];
    if (error) {
        safeExec(completion, nil, error);
        return;
    }
    
    MLKVisionImage *visionImage = [[MLKVisionImage alloc] initWithBuffer:sampleBuffer];
    visionImage.orientation = imageOrientation;
    
    WEAKSELF
    [self.faceDetector processImage:visionImage completion:^(NSArray<MLKFace *> * _Nullable faces, NSError * _Nullable error) {
        if (error) {
            KYC_LOG(@"Cannot detect face");
            safeExec(completion, nil, [KYCError errorWithCode:KYCErrorCodeUnKnown message:KYC_MESSAGE(@"Cannot detect face in image")]);
            return;
        }
        STRONGSELF
        if (faces.count == 0) {
            KYCError *error = [KYCError errorWithCode:KYCErrorCodeNonFace message:KYC_MESSAGE(@"The image doesn't have any faces")];
            safeExec(completion, nil, error);
        }
        else if (faces.count > 1) {
            KYCError *error = [KYCError errorWithCode:KYCErrorCodeMultipleFaces message:KYC_MESSAGE(@"The image have more than one face")];
            safeExec(completion, nil, error);
        }
        else {
            MLKFace *face = faces[0];
            KYCError *error = [self _validateFace:face fromImage:image];
            if (error) {
                safeExec(completion, nil, error);
            } else {
                KYCFace *kycFace = [[KYCFace alloc] initWithMLKFace:face originImage:image];
                safeExec(completion, kycFace, nil);
            }
        }
    }];
}

- (NSDictionary *)extractFeaturesWithUserInfo:(NSDictionary<id,UIImage *> *)userInfo {
    UIImage *leftFace = [userInfo objectForKey:@(KYCFaceOrientationLeft)];
    UIImage *rightFace = [userInfo objectForKey:@(KYCFaceOrientationRight)];
    UIImage *towardFace = [userInfo objectForKey:@(KYCFaceOrientationToward)];
    
    KYC_LOW_ASSERT(leftFace, @"userInfo doesn't have left face");
    KYC_LOW_ASSERT(rightFace, @"userInfo doesn't have right face");
    KYC_LOW_ASSERT(towardFace, @"userInfo doesn't have toward face");
    
    NSArray *leftFaceFeature = [self.faceNetModel extractFeaturesFromImage:leftFace];
    NSArray *rightFaceFeature = [self.faceNetModel extractFeaturesFromImage:rightFace];
    NSArray *towardFaceFeature = [self.faceNetModel extractFeaturesFromImage:towardFace];
    
    return @{
        @(KYCFaceOrientationLeft)   : leftFaceFeature,
        @(KYCFaceOrientationRight)  : rightFaceFeature,
        @(KYCFaceOrientationToward) : towardFaceFeature
    };
}

#pragma mark - Validating methods
- (KYCError *)_isSatisifiedBrightness:(UIImage *)image {
    CGFloat brightness = [image kyc_brightnessEstimate];
    if (brightness <= kLowBrightnessThreshold) {
        return [KYCError errorWithCode:KYCErrorCodeLowBrightnessImage message:@"Image have low brightness"];
    } else if (brightness >= kHightBrightnessThreshold) {
        return [KYCError errorWithCode:KYCErrorCodeHightBrightnessImage message:@"Image have hight brightness"];
    }
    return nil;
}

- (KYCError *)_validateFace:(MLKFace *)face fromImage:(UIImage *)image {
    /// Validate face out of area
    CGRect area = self.validateFrame;
    CGRect faceBound = [self _rectInCameraViewFromFaceRect:face.frame inImage:image];
    if (!CGRectContainsRect(area, faceBound)) {
        return [KYCError errorWithCode:KYCErrorCodeFaceOutOfArea message:@"Face is out of valid area"];
    }
    
    /// Validate distance from the face to camera
    CGFloat frameRadius = fmin(self.validateFrame.size.width, self.validateFrame.size.height) / 3;
    CGFloat faceArea = faceBound.size.width * faceBound.size.height;
    if (faceArea < M_PI * powf(frameRadius, 2)) {
        return [KYCError errorWithCode:KYCErrorCodeFaceDistanceCamera message:@"Distance from face and camera is too large"];
    }
    
    /// Validate face's eyes is opened
    if ((face.hasLeftEyeOpenProbability && face.leftEyeOpenProbability < kEyeOpenedThreshold)
        || (face.hasRightEyeOpenProbability && KYCFaceOrientationRight < kEyeOpenedThreshold)) {
        return [KYCError errorWithCode:KYCErrorCodeEyeNotOpened message:@"Face doesn't opened eyes"];
    }
    return nil;
}

/// MARK: - Helper methods
- (CGRect)_rectInCameraViewFromFaceRect:(CGRect)rect inImage:(UIImage *)image {
    CGFloat scaleW =  self.cameraFrame.size.width / image.size.width;
    CGFloat scaleH =  self.cameraFrame.size.height / image.size.height;
    CGFloat aspect = fmin(scaleW, scaleH);
    
    CGFloat imageOriginX = (self.cameraFrame.size.width-image.size.width * aspect)/2;
    CGFloat imageOriginY = (self.cameraFrame.size.height-image.size.height * aspect)/2;

    if (image.imageOrientation == UIImageOrientationLeft ||
        image.imageOrientation == UIImageOrientationRight ||
        image.imageOrientation == UIImageOrientationLeftMirrored ||
        image.imageOrientation == UIImageOrientationRightMirrored) {
        /// swap width with height, x with y
        return CGRectMake(rect.origin.y * aspect + imageOriginX,
                          rect.origin.x * aspect + imageOriginY,
                          rect.size.height * aspect,
                          rect.size.width * aspect);
    } else {
        return CGRectMake(rect.origin.y * aspect,
                          rect.origin.x * aspect,
                          rect.size.width * aspect,
                          rect.size.height * aspect);
    }
}

@end
