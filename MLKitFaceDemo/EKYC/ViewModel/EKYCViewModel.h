//
//  EKYCViewModel.h
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 14/07/2021.
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>
#import "KYCFace.h"
#import <AVFoundation/AVFoundation.h>
#import "KYCError.h"

NS_ASSUME_NONNULL_BEGIN

@interface EKYCViewModel : NSObject

- (instancetype)initWithCameraPosition:(AVCaptureDevicePosition)cameraPosition
                         validateFrame:(CGRect)faceArea
                           cameraFrame:(CGRect)cameraFrame;

- (void)faceWithCMSampleBuffer:(CMSampleBufferRef)sampleBuffer
                    completion:(void(^)(KYCFace * _Nullable face, KYCError * _Nullable error))completion;

/// Return the dictionary of features of all images in userInfo
/// @param userInfo the dictionary contain three image with difference orientations, dictionary with key is the KYCFaceOrientation
- (NSDictionary *)extractFeaturesWithUserInfo:(NSDictionary<id, UIImage *> *)userInfo;

@end

NS_ASSUME_NONNULL_END
