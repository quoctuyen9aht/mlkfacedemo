//
//  KYCResultViewController.h
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 15/07/2021.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    KYCResultTypeMatch,
    KYCResultTypeNotMatch,
} KYCResultType;

@interface KYCResultViewController : UIViewController

- (instancetype)initWithType:(KYCResultType)type;
@end

NS_ASSUME_NONNULL_END
