//
//  KYCResultViewController.m
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 15/07/2021.
//

#import "KYCResultViewController.h"

@interface KYCResultViewController ()

@property(nonatomic, strong) UIImageView *imageView;
@property(nonatomic, strong) UILabel *titleLabel;

@end

@implementation KYCResultViewController

- (instancetype)initWithType:(KYCResultType)type {
    self = [super init];
    if (self) {
        self.imageView = [[UIImageView alloc] init];
        self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        switch (type) {
            case KYCResultTypeMatch: {
                self.titleLabel.textColor = UIColor.greenColor;
                self.titleLabel.text = @"It's match!!";
                self.imageView.image = [UIImage imageNamed:@"match"];
            }
                break;
                
            case KYCResultTypeNotMatch: {
                self.titleLabel.textColor = UIColor.redColor;
                self.titleLabel.text = @"It's not match!!";
                self.imageView.image = [UIImage imageNamed:@"notmatch"];
            }
                break;
                
            default:
                break;
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.whiteColor;
    
    [self.view addSubview:self.imageView];
    [self.view addSubview:self.titleLabel];
    
    [NSLayoutConstraint activateConstraints:@[
        [self.imageView.widthAnchor constraintEqualToConstant:100],
        [self.imageView.heightAnchor constraintEqualToConstant:100],
        [self.imageView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:100],
        [self.imageView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor],
        
        [self.titleLabel.topAnchor constraintGreaterThanOrEqualToAnchor:self.imageView.bottomAnchor constant:16],
        [self.titleLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor],
        [self.titleLabel.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor]
    ]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
