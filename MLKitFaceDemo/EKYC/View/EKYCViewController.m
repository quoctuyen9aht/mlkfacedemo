//
//  EKYCViewController.m
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 14/07/2021.
//

#import "EKYCViewController.h"
#import "UIWindow+Utilities.h"
#import "EKYCViewModel.h"
#import "KYCMacros.h"
#import "UIImage+Utilities.h"
#import "KYCUserManager.h"
#import "KYCResultViewController.h"
@import AVFoundation;

#define kMaxProgress 2

@interface EKYCViewController () <AVCaptureVideoDataOutputSampleBufferDelegate>

@property(nonatomic, strong) AVCaptureSession *captureSession;
@property(nonatomic, assign) AVCaptureDevicePosition cameraPosition;
@property(nonatomic, strong) EKYCViewModel *viewModel;

@property(nonatomic, strong) UIStackView *stackView;
@property(nonatomic, strong) UIImageView *towardOutputImageView;
@property(nonatomic, strong) UIImageView *leftOutputImageView;
@property(nonatomic, strong) UIImageView *rightOutputImageView;

@property(nonatomic, strong) UIImage *leftFace;
@property(nonatomic, strong) UIImage *rightFace;
@property(nonatomic, strong) UIImage *towardFace;

@property(nonatomic, strong) UIView *overlayView;

@property(nonatomic, strong) UIView *testView;

@property(nonatomic, assign) BOOL isFinish;

@end

@implementation EKYCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    self.cameraPosition = AVCaptureDevicePositionFront;
    
    CGRect faceArea = CGRectMake((self.view.frame.size.width - 300) / 2,
                                 (self.view.frame.size.height - 300) / 2,
                                 300, 300);

    self.viewModel = [[EKYCViewModel alloc] initWithCameraPosition:self.cameraPosition
                                                          validateFrame:faceArea
                                                       cameraFrame:self.view.frame];
    [self _setupCamera];
    [self _setupView];
    [self _startRunning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _leftFace = nil;
    _rightFace = nil;
    _towardFace = nil;
    self.leftOutputImageView.image = nil;
    self.rightOutputImageView.image = nil;
    self.towardOutputImageView.image = nil;
    self.isFinish = NO;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    [self _startRunning];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self _stopRunning];
}

- (void)setLeftFace:(UIImage *)leftFace {
    if (!_leftFace) {
        _leftFace = leftFace;
        self.leftOutputImageView.image = leftFace;
    }
}

- (void)setRightFace:(UIImage *)rightFace {
    if (!_rightFace) {
        _rightFace = rightFace;
        self.rightOutputImageView.image = rightFace;
    }
}

- (void)setTowardFace:(UIImage *)towardFace {
    if (!_towardFace) {
        _towardFace = towardFace;
        self.towardOutputImageView.image = towardFace;
    }
}

- (UIView *)overlayView {
    if (!_overlayView) {
        _overlayView = [[UIView alloc] initWithFrame:self.view.frame];
        _overlayView.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0.5];
        
        CGMutablePathRef path = CGPathCreateMutable();
        
        CGFloat width = _overlayView.frame.size.width;
        CGFloat height = _overlayView.frame.size.height;
        
        CGPathAddArc(path, nil, width/2, height/2, 150, 0.0, 2 * M_PI, false);
        CGPathAddRect(path, nil, CGRectMake(0, 0, _overlayView.frame.size.width, _overlayView.frame.size.height));
        
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0.5].CGColor;
        maskLayer.path = path;
        maskLayer.fillRule = kCAFillRuleEvenOdd;
        _overlayView.layer.mask = maskLayer;
        _overlayView.clipsToBounds = YES;

    }
    return _overlayView;
}

- (void)_extractFeatureIfNeeded {
    if (!self.isFinish && self.leftFace && self.rightFace && self.towardFace) {
        self.isFinish = YES;
        NSDictionary *userInfo = @{
            @(KYCFaceOrientationLeft)   : self.leftFace,
            @(KYCFaceOrientationRight)  : self.rightFace,
            @(KYCFaceOrientationToward) : self.towardFace
        };
        NSDictionary *features = [self.viewModel extractFeaturesWithUserInfo:userInfo];
        
        BOOL isMatch = [[KYCUserManager shareManager] checkUserWithFeatures:features];
        
        UIViewController *resultVC = [[KYCResultViewController alloc] initWithType:isMatch ? KYCResultTypeMatch : KYCResultTypeNotMatch];
        [self.navigationController pushViewController:resultVC animated:YES];
    }
}

- (void)_setupView {
    self.stackView = [[UIStackView alloc] init];
    self.stackView.translatesAutoresizingMaskIntoConstraints = NO;
    self.stackView.distribution = UIStackViewDistributionFill;
    self.stackView.spacing = 16;
    self.stackView.axis = UILayoutConstraintAxisHorizontal;
    
    self.leftOutputImageView = [[UIImageView alloc] init];
    self.rightOutputImageView = [[UIImageView alloc] init];
    self.towardOutputImageView = [[UIImageView alloc] init];
    
    self.leftOutputImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.rightOutputImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.towardOutputImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    self.leftOutputImageView.backgroundColor = UIColor.lightGrayColor;
    self.rightOutputImageView.backgroundColor = UIColor.lightGrayColor;
    self.towardOutputImageView.backgroundColor = UIColor.lightGrayColor;
    
    [self.view addSubview:self.overlayView];
    [self.view addSubview:self.stackView];
    [self.stackView addArrangedSubview:self.leftOutputImageView];
    [self.stackView addArrangedSubview:self.towardOutputImageView];
    [self.stackView addArrangedSubview:self.rightOutputImageView];
    
    [NSLayoutConstraint activateConstraints:@[
        [self.stackView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:-50],
        [self.stackView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant:8],
        [self.stackView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-8],
        [self.stackView.heightAnchor constraintEqualToAnchor:self.view.widthAnchor multiplier:1.0/3 constant:-16],
        
        [self.leftOutputImageView.widthAnchor constraintEqualToConstant:(self.view.frame.size.width - 16*3) / 3.0],
        [self.rightOutputImageView.widthAnchor constraintEqualToConstant:(self.view.frame.size.width - 16*3) / 3.0],
        [self.towardOutputImageView.widthAnchor constraintEqualToConstant:(self.view.frame.size.width - 16*3) / 3.0]
    ]];
}

- (void)_setupCamera {
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInWideAngleCamera
                                                                        mediaType:AVMediaTypeVideo
                                                                         position:self.cameraPosition];
    self.captureSession = [[AVCaptureSession alloc] init];
    self.captureSession.sessionPreset = AVCaptureSessionPresetPhoto;
    
    if (captureDevice) {
        NSError *error = nil;
        AVCaptureVideoDataOutput *captureOutput = [[AVCaptureVideoDataOutput alloc] init];
        [captureOutput setSampleBufferDelegate:self queue:dispatch_queue_create("kyc.videoqueue", DISPATCH_QUEUE_SERIAL)];
        AVCaptureDeviceInput *captureInput = [[AVCaptureDeviceInput alloc] initWithDevice:captureDevice
                                                                                    error:&error];
        
        if (!error && [self.captureSession canAddInput:captureInput]
            && [self.captureSession canAddOutput:captureOutput]) {
            [self.captureSession addInput:captureInput];
            [self.captureSession addOutput:captureOutput];
            
            AVCaptureVideoPreviewLayer *previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:self.captureSession];
            if (previewLayer) {
                previewLayer.videoGravity = AVLayerVideoGravityResizeAspect;
                previewLayer.frame = self.view.frame;
                previewLayer.connection.videoOrientation = [self _orientationBaseOnDevice];
                [self.view.layer insertSublayer:previewLayer atIndex:0];
            }
        }
    }
}

- (void)_startRunning {
    if (!self.captureSession.isRunning) {
        [self.captureSession startRunning];
    }
}

- (void)_stopRunning {
    if (self.captureSession.isRunning && self.captureSession.inputs.count > 0) {
        [self.captureSession stopRunning];
    }
}

- (AVCaptureVideoOrientation)_orientationBaseOnDevice {
    UIInterfaceOrientation currentOrientation = [UIWindow kyc_interfaceOrientation];
    switch (currentOrientation) {
        case UIInterfaceOrientationPortrait:
            return AVCaptureVideoOrientationPortrait;
        case UIInterfaceOrientationPortraitUpsideDown:
            return AVCaptureVideoOrientationPortraitUpsideDown;
        case UIInterfaceOrientationLandscapeLeft:
            return AVCaptureVideoOrientationLandscapeLeft;
        case UIInterfaceOrientationLandscapeRight:
            return AVCaptureVideoOrientationLandscapeRight;
        default:
            return AVCaptureVideoOrientationPortrait;
    }
}

#pragma mark - AVCaptureVideoDataOutputSampleBufferDelegate methods
- (void)captureOutput:(AVCaptureOutput *)output didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection {
    
    WEAKSELF
    [self.viewModel faceWithCMSampleBuffer:sampleBuffer
                                completion:^(KYCFace * _Nullable face, KYCError * _Nullable error) {
        STRONGSELF
        if (error) {
            [self handleError:error];
        } else {
            switch (face.orientation) {
                case KYCFaceOrientationToward:
                    [self setTowardFace:face.image];
                    break;
                case KYCFaceOrientationLeft:
                    [self setLeftFace:face.image];
                    break;
                case KYCFaceOrientationRight:
                    [self setRightFace:face.image];
                    break;
                default:
                    break;
            }
            
            [self _extractFeatureIfNeeded];
        }
    }];
}

/// Handle error here, we can show toast to guide user or just printing message to log
/// @param error error from KYCFace
- (void)handleError:(KYCError *)error {
    KYC_LOG(@"%@", error.message);
//    switch (error.code) {
//        case KYCErrorCodeLowBrightnessImage:
//
//            break;
//
//        default:
//            break;
//    }
    
}

@end
