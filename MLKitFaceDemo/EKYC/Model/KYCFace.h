//
//  KYCFace.h
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 14/07/2021.
//

#import <UIKit/UIKit.h>
@import MLKitFaceDetection;

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    KYCFaceOrientationUnknown,
    KYCFaceOrientationLeft,
    KYCFaceOrientationRight,
    KYCFaceOrientationToward
} KYCFaceOrientation;

@interface KYCFace : NSObject

- (instancetype)initWithMLKFace:(MLKFace *)face
                    originImage:(UIImage *)image;
/// The face image, which croped from origin and just contain faces
@property(nonatomic, strong, readonly) UIImage *image;
/// Orientation of face
@property(nonatomic, assign, readonly) KYCFaceOrientation orientation;

@end

NS_ASSUME_NONNULL_END
