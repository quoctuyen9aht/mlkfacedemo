//
//  KYCFace.m
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 14/07/2021.
//

#import "KYCFace.h"
#import "UIImage+Utilities.h"

#define valueInAbout(_value, _start, _end) (_value >= _start && _value <= _end)

@interface KYCFace ()
@property(nonatomic, strong) UIImage *image;
@property(nonatomic, assign) KYCFaceOrientation orientation;
@end

@implementation KYCFace

- (instancetype)initWithMLKFace:(MLKFace *)face originImage:(UIImage *)image {
    self = [super init];
    if (self) {
        self.orientation = [self _faceOrientationFromMLKFace:face];
        self.image = [UIImage kyc_cropImageFromImage:image toRect:face.frame];
    }
    return self;
}

- (KYCFaceOrientation)_faceOrientationFromMLKFace:(MLKFace *)face {
    CGFloat eulerY = face.headEulerAngleY;
    if (valueInAbout(eulerY, -12, 12)) {
        return KYCFaceOrientationToward;
    } else if (valueInAbout(eulerY, -36, -12)) {
        return KYCFaceOrientationLeft;
    } else if (valueInAbout(eulerY, 12, 36)) {
        return KYCFaceOrientationRight;
    } else {
        return KYCFaceOrientationUnknown;
    }
}

@end
