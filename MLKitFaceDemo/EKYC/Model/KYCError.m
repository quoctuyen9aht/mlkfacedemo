//
//  KYCError.m
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 14/07/2021.
//

#import "KYCError.h"

#define kUserInfoMessageKey  @"kycMessage"

@implementation KYCError

+ (instancetype)errorWithCode:(KYCErrorCode)code
                     userInfo:(NSDictionary<NSErrorUserInfoKey,id> *)userInfo {
    return [self errorWithDomain:KYCErrorDomain
                            code:code
                        userInfo:userInfo];
}

+ (instancetype)errorWithCode:(KYCErrorCode)code message:(NSString *)message {
    return [self errorWithDomain:KYCErrorDomain
                            code:code
                        userInfo:@{kUserInfoMessageKey : message}];
}

- (NSString *)message {
    return [self.userInfo objectForKey:kUserInfoMessageKey];
}

@end
