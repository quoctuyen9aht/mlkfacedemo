//
//  KYCError.h
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 14/07/2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, KYCErrorCode) {
    KYCErrorCodeUnKnown = 100,
    KYCErrorCodeNonFace,
    KYCErrorCodeMultipleFaces,
    KYCErrorCodeLowQualityImage,
    KYCErrorCodeEyeNotOpened,
    KYCErrorCodeLowBrightnessImage,
    KYCErrorCodeHightBrightnessImage,
    KYCErrorCodeFaceOutOfArea,
    KYCErrorCodeFaceDistanceCamera
};

static const NSErrorDomain KYCErrorDomain = @"error.kyc";

@interface KYCError : NSError

@property(nonatomic, nullable, strong, readonly) NSString *message;

+ (instancetype)errorWithCode:(KYCErrorCode)code
                     userInfo:(nullable NSDictionary<NSErrorUserInfoKey, id> *)userInfo;

+ (instancetype)errorWithCode:(KYCErrorCode)code
                      message:(nullable NSString *)message;

@end

NS_ASSUME_NONNULL_END
