//
//  KYCUserManager.m
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 15/07/2021.
//

#import "KYCUserManager.h"
#import "KYCFace.h"

#define euclidThreshold     0.95

@interface KYCUserManager ()
@property(nonatomic, strong, readwrite) NSDictionary<id, NSArray *> *features;
@end

@implementation KYCUserManager

+ (KYCUserManager *)shareManager {
    static KYCUserManager *shareManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareManager = [[KYCUserManager alloc] init];
    });
    return shareManager;
}

- (BOOL)checkUserWithFeatures:(NSDictionary<id,NSArray *> *)features {
    if (self.features) {
        BOOL isMatchLeftFace = [self compareFeature:[features objectForKey:@(KYCFaceOrientationLeft)]
                                        withFeature:[self.features objectForKey:@(KYCFaceOrientationLeft)]];
        BOOL isMatchRightFace = [self compareFeature:[features objectForKey:@(KYCFaceOrientationRight)]
                                        withFeature:[self.features objectForKey:@(KYCFaceOrientationRight)]];
        BOOL isMatchTowardFace = [self compareFeature:[features objectForKey:@(KYCFaceOrientationToward)]
                                        withFeature:[self.features objectForKey:@(KYCFaceOrientationToward)]];
        
        return isMatchLeftFace && isMatchRightFace && isMatchTowardFace;
    } else {
        self.features = features;
        return NO;
    }
}

- (BOOL)compareFeature:(NSArray *)feature1 withFeature:(NSArray *)feature2 {
    if (feature1.count == feature2.count) {
        CGFloat total = 0;
        for (int i = 0; i < feature1.count; i++) {
            CGFloat x = [((NSNumber *)feature1[i]) floatValue];
            CGFloat y = [((NSNumber *)feature2[i]) floatValue];
            total += powf(x - y, 2);
        }
        CGFloat euclid = sqrtf(total);
        return euclid <= euclidThreshold;
    } else {
        return NO;
    }
}

@end
