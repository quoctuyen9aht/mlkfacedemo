//
//  ViewController.m
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 12/07/2021.
//

#import "ViewController.h"
#import "EKYCViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
}

- (IBAction)ekycAction:(id)sender {
    EKYCViewController *vc = [[EKYCViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
