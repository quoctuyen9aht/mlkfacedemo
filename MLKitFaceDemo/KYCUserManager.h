//
//  KYCUserManager.h
//  MLKitFaceDemo
//
//  Created by Nguyen Quoc Tuyen on 15/07/2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KYCUserManager : NSObject

@property(class, nonatomic, strong, readonly) KYCUserManager *shareManager;

- (BOOL)checkUserWithFeatures:(NSDictionary<id, NSArray *> *)features;

@end

NS_ASSUME_NONNULL_END
